# Java Base-Project Using Gradle

## About this project
This repository aims to provide a basic Java project using Gradle. Included in the toolchain are several analysis tools:

* [PMD](https://pmd.github.io/)
* [SpotBugs](https://spotbugs.github.io/)
* [CPD](https://pmd.github.io/latest/pmd_userdocs_cpd.html)
* [Checkstyle](https://checkstyle.org/)
* [JaCoCo](https://www.jacoco.org/jacoco/)

There exist also several configuration files for them in the config folder that are automatically included during the corresponding Gradle tasks.

## How to set up this project
This project is already ready to use but given that you probably want to use your own structure, names and dependencies you should have a look and modify the following files:
* `settings.gradle`  
  Add your projects name here
* `build.gradle`  
  - Add all your custom repositories in the repositories-block as needed
  - Add your own dependencies to the dependencies-block
  - Set group according to your project
  - Set version number (do not forget to update this)
  - Set gradleVersion in the wrapper task to create wrapper by default with specific version
  - Set sourceCompatibility/targetCompatibility to the Java versions that you use and want to support
  - In the jar-task at the bottom add the path (including package) to your entry point (usually the class where your main() is) and define a title
* Change the package/folder structure under `src/{java,test}` according to the group that you defined in the `build.gradle` and the project name
  
## Build project
If you want to build the project for the first time run `gradle wrapper` inside the root directory first. 
This will create a wrapper with a fixed version in a folder called `gradle` which allows users to build the code without having to install Gradle as well.
From now on you can use either `./gradlew` or `gradlew.bat` depending on your OS to run tasks.  
Now to compile the code, run all checks, tests and create a jar you can use `./gradlew build`.   
If you only want to run the JUnit-tests you can use `./gradlew test` instead.  
For a complete list of tasks use the command `./gradlew tasks`.  