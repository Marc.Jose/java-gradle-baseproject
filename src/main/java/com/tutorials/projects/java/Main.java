package com.tutorials.projects.java;

public final class Main {

  private Main() {
    // Nothing to do here
  }

  /**
   * Main method which is given the command line arguments and serves as an
   * entry point for the whole application.
   * @param args array of command line arguments
   */
  public static void main(final String[] args) {
    // TODO Implement here
  }

}
